

#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>

#include "world.h"
#include "read_line.h"
#include "add_stuff.h"
#include "icons.h"
#include "load_lists.h"
#include "save_lists.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


void dismiss_notice (GtkWidget *widget, GtkWidget *dialog)
{
  gtk_widget_destroy (dialog);
}


void show_notice (const gchar* text, GtkWidget *parent)
{
  GtkWidget *dialog, *table, *label, *button;
  GString *string;

  string = g_string_new (NULL);    
  g_string_sprintf (string, "%s message", PACKAGE);
  dialog = gtk_dialog_new_with_buttons (string->str,
    GTK_WINDOW (parent), GTK_DIALOG_NO_SEPARATOR, NULL);
  if (parent)
    gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
  gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (dialog), 5);

  table = gtk_table_new (2, 3, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), table);

  label = gtk_label_new (text);
  gtk_table_attach (GTK_TABLE (table), label, 0, 3, 0, 1,
    GTK_SHRINK, GTK_SHRINK, 0, 5);
    
  button = gtk_button_new_with_label ("OK");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (dismiss_notice),
    dialog); 
  gtk_table_attach (GTK_TABLE (table), button, 1, 2, 2, 3,
    GTK_SHRINK, GTK_SHRINK, 0, 5);
      
  gtk_widget_show_all (dialog);
}


void cancel_browse (GtkWidget *widget, world_info *info)
{
  gtk_widget_destroy (info->browse_widget);
}


void cancel_edits (GtkWidget *widget, world_info *info)
{
  gtk_widget_destroy (info->edits_dialog);
}


void accept_location (GtkWidget *widget, world_info *info)
{
  GString *string;
  const gchar *text, *dir, *dir_slash, *cmd;
  gint diff, offset;
  
  text = gtk_file_selection_get_filename (GTK_FILE_SELECTION (
    info->browse_widget));
  dir = find_parent_dir (text);
  dir_slash = find_parent_dir_slash (text);

  if (info->browse_target == bt_cmd)
  {
    g_string_assign (info->cmd_dir, dir_slash);
    diff = strlen (text) - strlen (dir_slash);
    if (diff > 0 && diff < strlen (text))
    {
      gtk_entry_set_text (GTK_ENTRY (info->dir_field), dir);
      string = g_string_new (NULL);
      offset = strlen (text) - diff;
      cmd = &text [offset];
      g_string_sprintf (string, "./%s", cmd);
      gtk_entry_set_text (GTK_ENTRY (info->cmd_field), string->str);
    }
    else
    {
      gtk_entry_set_text (GTK_ENTRY (info->dir_field), "");
      gtk_entry_set_text (GTK_ENTRY (info->cmd_field), text);
    }
  }
  else
  if (info->browse_target == bt_icon)
  {
    gtk_entry_set_text (GTK_ENTRY (info->icon_field), text);
    g_string_assign (info->icon_dir, find_parent_dir_slash (text));
    if (info->icon_image)
      gtk_widget_destroy (info->icon_image);
    info->icon_image = scaled_icon (text);
    if (info->icon_image)
    {
      gtk_table_attach (GTK_TABLE (info->edits_table), info->icon_image,
        2, 3, 4, 5, GTK_SHRINK, GTK_SHRINK, info->icon_x_spacing,
        info->icon_y_spacing);
      gtk_widget_show (info->icon_image);
    }
  }
  else
    g_print ("(debug) world: unknown browse target\n");
  gtk_widget_destroy (info->browse_widget);
}


void browse_for_file (world_info *info)
{
  GString *dir_name;
  
  if (info->browse_target == bt_cmd)
  {
    info->browse_widget = gtk_file_selection_new ("Program Location");
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (info->browse_widget),
      info->cmd_dir->str);
  }
  else
  if (info->browse_target == bt_icon)
  {
    info->browse_widget = gtk_file_selection_new ("Icon Location");
    dir_name = g_string_new (NULL);
    g_string_sprintf (dir_name, "%s/", info->icon_dir->str);
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (info->browse_widget),
      dir_name->str);
  }
  else
  {
    g_print ("(debug) world: unknown browse target\n");
    return;
  }
  
  gtk_window_set_transient_for (GTK_WINDOW (info->browse_widget),
    GTK_WINDOW (info->edits_dialog));
  gtk_window_set_modal (GTK_WINDOW (info->browse_widget), TRUE);
  g_signal_connect (G_OBJECT (info->browse_widget), "destroy",
    G_CALLBACK (cancel_browse), info);
  g_signal_connect (G_OBJECT (
    GTK_FILE_SELECTION (info->browse_widget)->ok_button),
    "clicked", G_CALLBACK (accept_location), info);
  g_signal_connect (G_OBJECT (
    GTK_FILE_SELECTION (info->browse_widget)->cancel_button),
    "clicked", G_CALLBACK (cancel_browse), info);
  gtk_widget_show (info->browse_widget);
}


void browse_for_cmd (GtkWidget *widget, world_info *info)
{
  info->browse_target = bt_cmd;
  browse_for_file (info);
}


void browse_for_icon (GtkWidget *widget, world_info *info)
{
  info->browse_target = bt_icon;
  browse_for_file (info);
}


void save_clicked (GtkWidget *widget, world_info *info)
{
  cell_struct *cell;
  item_struct *item;
  GString *new_name;

  cell = info->current_cell;
  item = cell->item;
  new_name = g_string_new (gtk_entry_get_text (GTK_ENTRY (info->name_field)));
  if (!text_here (new_name))
  {
    gtk_widget_destroy (info->edits_dialog);
    return;
  }

  hide_cell_visibles (cell);
  g_string_assign (item->name,
    gtk_entry_get_text (GTK_ENTRY (info->name_field)));
  g_string_assign (item->dir,
    gtk_entry_get_text (GTK_ENTRY (info->dir_field)));
  g_string_assign (item->cmd,
    gtk_entry_get_text (GTK_ENTRY (info->cmd_field)));
  g_string_assign (item->icon,
     gtk_entry_get_text (GTK_ENTRY (info->icon_field)));

  pinup_item (cell);

  gtk_widget_destroy (info->edits_dialog);
  save_all (info);
}


void new_text_field (world_info *info, gint row, const gchar *text_label,
  GtkWidget **field, const gchar *initial)
{
  GtkWidget *label;

  label = gtk_label_new (text_label);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_table_attach_defaults (GTK_TABLE (info->edits_table), label, 0, 1,
    row, row + 1);
  
  *field = gtk_entry_new();
  gtk_entry_set_text (GTK_ENTRY (*field), initial);
  gtk_table_attach_defaults (GTK_TABLE (info->edits_table), *field, 2, 3,
    row, row + 1);
}


void edits_dialog (cell_struct *cell)
{
  GtkWidget *button;
  world_info *info;
  item_struct *item;

  info = (world_info*) cell->info;
  info->current_cell = cell;
  item = cell->item;
  info->edits_dialog = gtk_dialog_new_with_buttons ("Edit",
      GTK_WINDOW (info->main_window), GTK_DIALOG_NO_SEPARATOR, NULL);
  
  gtk_window_set_transient_for (GTK_WINDOW (info->edits_dialog),
    GTK_WINDOW (info->main_window));
  gtk_window_set_modal (GTK_WINDOW (info->edits_dialog), TRUE);
  gtk_container_set_border_width (GTK_CONTAINER (info->edits_dialog),
    info->button_spacing);
  info->edits_table = gtk_table_new (6, 4, FALSE);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (info->edits_dialog)->vbox),
    info->edits_table);
  gtk_table_set_row_spacings (GTK_TABLE (info->edits_table),
    info->button_spacing);
  gtk_table_set_col_spacings (GTK_TABLE (info->edits_table),
    info->button_spacing);

  new_text_field (info, 0, "Name:", &info->name_field, item->name->str);
  new_text_field (info, 1, "Dir:", &info->dir_field, item->dir->str);
  new_text_field (info, 2, "Command:", &info->cmd_field, item->cmd->str);
  new_text_field (info, 3, "Icon:", &info->icon_field, item->icon->str);
  
  button = gtk_button_new_with_label ("OK");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (save_clicked),
    info); 
  gtk_table_attach (GTK_TABLE (info->edits_table), button, 3, 4, 0, 1,
    GTK_FILL, GTK_SHRINK, 0, 0);
  
  button = gtk_button_new_with_label ("Browse");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (browse_for_cmd),
    info); 
  gtk_table_attach (GTK_TABLE (info->edits_table), button, 3, 4, 1, 3,
    GTK_FILL, GTK_SHRINK, 0, 0);
  
  button = gtk_button_new_with_label ("Browse");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (browse_for_icon),
    info); 
  gtk_table_attach (GTK_TABLE (info->edits_table), button, 3, 4, 3, 4,
    GTK_FILL, GTK_SHRINK, 0, 0);
  
  info->icon_image = gtk_image_new_from_file (item->icon->str);
  gtk_table_attach (GTK_TABLE (info->edits_table), info->icon_image,
    2, 3, 4, 5, GTK_SHRINK, GTK_SHRINK,
    info->icon_x_spacing, info->icon_y_spacing);
    
  button = gtk_button_new_with_label ("Cancel");
  g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (cancel_edits),
    info); 
  gtk_table_attach_defaults (GTK_TABLE (info->edits_table), button,
    3, 4, 5, 6);
      
  gtk_widget_show_all (info->edits_dialog);
}

