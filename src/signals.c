
#include <signal.h>
#include <gtk/gtk.h>
#include "world.h"
#include "load_lists.h"
#include "choices.h"

const int SIGWORLD = 50;

static world_info *info = NULL;


void wake (int code)
{
  if (info && code == SIGWORLD)
  {
    gtk_widget_show_all (GTK_WIDGET (info->main_window));
    if (info->world_revert_to_first)
      info->last_tab = 0;
    gtk_notebook_set_current_page (GTK_NOTEBOOK (info->tabs), info->last_tab);
    show_edit_buttons (info, FALSE);
    set_window (info);
    /* the following line doesn't always work? Give suggestions! */
    gtk_window_set_focus (GTK_WINDOW (info->main_window), NULL);
  }
}


void signal_init (world_info *pointer)
{
  info = pointer;
  signal (SIGWORLD, wake);
}
