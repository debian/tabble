
#ifndef WORLD
#define WORLD

#include <gtk/gtk.h>

static const gchar default_window_name[] = "tabble";
static const gchar default_icon_dir[] = ".";
static const gchar default_cmd_dir[] = ".";
static const gint icon_width = 48;
static const gint icon_height = 48;

enum browse_targets {bt_cmd, bt_icon};

typedef struct
{
  GString *name, *dir, *cmd, *icon;
  gint x_pos, y_pos;
} item_struct;

typedef struct
{
  struct world_info *info;
  item_struct *item;
  gint tab_pos;
  gboolean filled;
  GtkWidget *icon_image, *icon_button, *button, *label;
} cell_struct;

typedef struct
{
  GString *tab_name;
  GtkWidget *box_widget;
  cell_struct *cell_array;
} tab_struct;

typedef struct
{
  GtkWidget *main_window, *tabs;
  GtkWidget *edits_dialog, *edits_table, *browse_widget;
  GtkWidget *name_field, *dir_field, *cmd_field, *icon_field, *icon_image;
  GtkWidget *options_dialog, *options_table;
  GtkWidget *options_field_1, *options_field_2;
  GtkWidget *options_check_1, *options_check_2, *options_check_3;

  GList *tab_list;
  gint tab_count, last_tab;
  gboolean run_again, blanks_shown;
  
  gint argc;
  gchar **argv;
  GString *conf_file, *icon_dir, *cmd_dir, *pwd_dir, *window_name;
  gint max_line;
  
  gint button_spacing, icon_x_spacing, icon_y_spacing, frame_spacing;
  gint x_icons, y_icons, x_icons_new, y_icons_new;
  
  gboolean world_on_top, world_sticky, world_revert_to_first, options_enabled;
  gint start_x, start_y, start_w, start_h;
  
  cell_struct *current_cell, *selected_cell, *pressed_cell, *entered_cell;
  int browse_target;
} world_info;


#endif
