
#include <stdio.h>
#include <string.h>

#include "world.h"
#include "edits.h"
#include "read_line.h"
#include "add_stuff.h"


typedef struct
{
  FILE *file;
  GString *error;
} save_struct;


void save_line (save_struct *context, const gchar *text)
{
  size_t actual, count;
  
  if (text_here (context->error))
    return;
  
  actual = strlen (text);
  if (actual <= 0)
    return;
  
  count = fwrite (text, 1, strlen (text), context->file);
  if (count != actual)
    g_string_assign (context->error, "Could not write all of buffer");
}


void save_value (save_struct *context, const gchar *key, const GString *value)
{
  GString *line;
  
  if (!text_here (value))
    return;
  
  line = g_string_new (NULL);
  g_string_sprintf (line, "%s: %s\n", key, value->str);
  save_line (context, line->str);
  g_string_free (line, TRUE);
}


gchar* bool_text (gboolean value)
{
  if (value)
    return "true";
  else
    return "false";
}


const gchar* save_run (world_info *info, gboolean new_grid)
{
  tab_struct *tab;
  cell_struct *cell;
  item_struct *item;
  save_struct context;
  gint x, y, t, new_x, new_y;
  GString *line, *icon_save_dir;

  if (!text_here (info->conf_file))
    return "No save file";

  context.file = fopen (info->conf_file->str, "w");
  if (!context.file)
    return "No write permission";
  
  context.error = g_string_new (NULL);
  line = g_string_new (NULL);
  icon_save_dir = g_string_new ("");

  g_string_sprintf (line, "window name: %s\n", info->window_name->str);
  save_line (&context, line->str);
  g_string_sprintf (line, "options: %s\n", bool_text (info->options_enabled));
  save_line (&context, line->str);
  g_string_sprintf (line, "on top: %s\n", bool_text (info->world_on_top));
  save_line (&context, line->str);
  g_string_sprintf (line, "sticky: %s\n", bool_text (info->world_sticky));
  save_line (&context, line->str);
  g_string_sprintf (line, "revert: %s\n",
    bool_text (info->world_revert_to_first));
  save_line (&context, line->str);
  g_string_sprintf (line, "start x: %d\n", info->start_x);
  save_line (&context, line->str);
  g_string_sprintf (line, "start y: %d\n", info->start_y);
  save_line (&context, line->str);
  g_string_sprintf (line, "start w: %d\n", info->start_w);
  save_line (&context, line->str);
  g_string_sprintf (line, "start h: %d\n", info->start_h);
  save_line (&context, line->str);
  if (new_grid)
  {
    g_string_sprintf (line, "x icons: %d\n", info->x_icons_new);
    save_line (&context, line->str);
    g_string_sprintf (line, "y icons: %d\n", info->y_icons_new);
    save_line (&context, line->str);
  }
  else
  {
    g_string_sprintf (line, "x icons: %d\n", info->x_icons);
    save_line (&context, line->str);
    g_string_sprintf (line, "y icons: %d\n", info->y_icons);
    save_line (&context, line->str);
  }
  save_line (&context, "\n");
  
  for (t = 0; t < info->tab_count; t++)
  {
    new_x = new_y = 0;
    tab = (tab_struct*) g_list_nth (info->tab_list, t)->data;
    g_string_sprintf (line, "tab: %s\n\n", tab->tab_name->str);
    save_line (&context, line->str);
    
    for (y = 0; y < info->y_icons; y++)
    {
      for (x = 0; x < info->x_icons; x++)
      {
        cell = get_grid (info, t, x, y);
        if (cell->filled)
        {
          item = cell->item;
          save_value (&context, "name", item->name);
          if (text_here (item->dir))
            save_value (&context, "dir", item->dir);
          if (text_here (item->cmd))
            save_value (&context, "cmd", item->cmd);
          if (text_here (item->icon))
          {
            if (strcmp (find_parent_dir (item->icon->str), icon_save_dir->str))
            {
              g_string_assign (icon_save_dir,
                find_parent_dir (item->icon->str));
              save_value (&context, "icon dir", icon_save_dir);
            }
            save_value (&context, "icon",
              g_string_new (find_filename (item->icon->str)));
          }
          if (new_grid)
          {
            g_string_sprintf (line, "x pos: %d\n", new_x);
            save_line (&context, line->str);
            g_string_sprintf (line, "y pos: %d\n", new_y);
            save_line (&context, line->str);
            new_x++;
            if (new_x >= info->x_icons_new)
            {
              new_x = 0;
              new_y++;
            }
          }
          else
          {
            g_string_sprintf (line, "x pos: %d\n", item->x_pos);
            save_line (&context, line->str);
            g_string_sprintf (line, "y pos: %d\n", item->y_pos);
            save_line (&context, line->str);
          }
          save_line (&context, "\n");
        }
        if (text_here (context.error))
          break;
      }
      if (text_here (context.error))
        break;
    }
    if (text_here (context.error))
      break;
  }

  fclose (context.file);
  if (text_here (context.error))
    return context.error->str;
  else
    return NULL;
}


void save_all (world_info *info)
{
  GString *message;
  const gchar *error;
  
  error = save_run (info, FALSE);
  if (error)
  {
    message = g_string_new (NULL);
    g_string_sprintf (message, "This error occured during save:\n\n%s", error);
    show_notice (message->str, info->main_window);
  }
}


void save_all_compressed (world_info *info)
{
  GString *message;
  const gchar *error;
  
  error = save_run (info, TRUE);
  if (error)
  {
    message = g_string_new (NULL);
    g_string_sprintf (message, "This error occured during save:\n\n%s", error);
    show_notice (message->str, info->main_window);
  }
}
