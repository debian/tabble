
#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>

#include "read_line.h"


gboolean text_here (const GString *text)
{
  if ((text == NULL) || (strcmp (text->str, "") == 0))
    return 0;
  else
    return 1;
}


void free_g_string (GString *text)
{
  if (text_here (text))
      g_string_free (text, TRUE);
}


const gchar* find_parent_dir (const gchar *source)
{
  GString *dest;
  gint index;
  
  dest = g_string_new ("/");
  
  index = strlen (source);
  if (index <= 1)
    return dest->str;
  
  index--;
  for (;;)
  {
    if (source [index] != '/')
      break;
    index--;
  }
  
  for (;;)
  {
    if (source [index] == '/')
    {
      dest = g_string_new_len (source, index);
      return dest->str;
    }
    index--;
    if (index <= 0)
      return dest->str;
  }  
}

const gchar* find_parent_dir_slash (const gchar *source)
{
  GString *result;
  const gchar *parent;
  
  parent = find_parent_dir (source);
  if (strlen (parent) <= 1)
    return parent;
  
  result = g_string_new (NULL);
  g_string_sprintf (result, "%s/", parent);
  return result->str;
}


const gchar* find_filename (const gchar *source)
{
  gint index;
  
  index = strlen (source);
  
  while (index > 0)
  {
    if (source [index] == '/')
      return g_string_new (source + index + 1)->str;
    index--;
  }

  return g_string_new (source)->str;  
}


int char_find (const gchar* text, char sought)
{
  int index = 0;
  gchar letter;
  
  for (;;)
  {
    letter = text [index]; /* yeah a bit dangerous */
    if (letter == '\0')
      return -1;
    if (letter == sought)
      return index;
    index++;
  }
}


readline* read_start (const gchar* filename, gint max_length)
{
  readline* context;
  
  context = g_malloc0 (sizeof (readline));
  context->filename = g_string_new (filename);
  context->pos = 0;
  context->max_length = max_length;
  
  context->file = fopen (filename, "r");
  if (!context->file)
  {
    g_print ("Can't read %s (exists?)\n", filename);
    return NULL;
  }
  
  context->buffer = g_malloc0 (max_length + 4);
  context->pos = 0;
  return context;
}


gboolean read_next (readline *context, GString **destination)
{
  gint count, index;
  
  if (context->pos < 0)
    return FALSE;

  fseek (context->file, context->pos, SEEK_SET);
  count = fread (context->buffer, 1, context->max_length, context->file);
  if (count <= 0)
  {
    g_free (context->buffer);
    fclose (context->file);
    context->pos = -1;
    return FALSE;
  }
  context->buffer [context->max_length + 1] = '\0';
  index = char_find (context->buffer, '\n');
  if (index < 0)
  {
    g_print ("Line too long\n");
    g_free (context->buffer);
    fclose (context->file);
    context->pos = -1;
    return FALSE;
  }
  context->buffer [index] = '\0';
  g_string_assign (*destination, context->buffer);
  context->pos += index + 1;
  return TRUE;
}


gboolean file_readable (const gchar *filename)
{
  FILE *file;
  
  file = fopen (filename, "r");
  if (file)
  {
    fclose (file);
    return TRUE;
  }
  else
    return FALSE;
}


const gchar* cmdline_option (world_info *info, const gchar *option)
{
  gint count, found;
  gchar **argv = info->argv;
  static const gchar empty[] = "";
  
  found = -1;
  for (count = 0; count < info->argc; count++)
    if (strcmp (argv [count], option) == 0)
    {
      found = count;
      break;
    }
  if (found < 0)
    return NULL;
  if (found < info->argc - 1)
    return argv [found + 1];
  return empty;
}
