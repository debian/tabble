
#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "world.h"


GtkWidget* any_scale_icon (const gchar *filename, gint width, gint height)
{
  GdkPixbuf *pixbuf, *scaled;
  GtkWidget *result;
  GError *error = NULL;
  float max_x, max_y, icon_x, icon_y, max_ratio, icon_ratio, new_x, new_y;

  pixbuf = gdk_pixbuf_new_from_file (filename, &error);
  if (error != NULL)
    return NULL;

  icon_x = gdk_pixbuf_get_width (pixbuf);
  icon_y = gdk_pixbuf_get_height (pixbuf);
  max_x = width;
  max_y = height;
  
  if ((max_x <= 0) || (max_y <=0) || ((icon_x == max_x) && (icon_y == max_y)))
  {
    result = gtk_image_new_from_pixbuf (pixbuf);
    g_object_unref (pixbuf) ;
    return result;
  }
  
  max_ratio = max_y / max_x;
  icon_ratio = icon_y / icon_x;
  
  if (max_ratio >= icon_ratio)
  {
    new_x = max_x;
    new_y = max_y * icon_ratio / max_ratio;
  }
  else
  {
    new_x = max_x * max_ratio / icon_ratio;
    new_y = max_y;
  }
  
  scaled = gdk_pixbuf_scale_simple (pixbuf, (gint) new_x, (gint) new_y,
    GDK_INTERP_BILINEAR);
  result = gtk_image_new_from_pixbuf (scaled);
  g_object_unref (pixbuf);
  g_object_unref (scaled);
  return result;
}


GtkWidget* scaled_icon (const gchar *filename)
{
  return any_scale_icon (filename, icon_width, icon_height);
}
