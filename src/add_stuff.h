

#include "world.h"


gint add_tab (world_info *info, const gchar* name);
void del_tab (world_info *info, gint tab_number);
void hide_cell_visibles (cell_struct *cell);
void zero_item (item_struct *item);
void free_item (item_struct *item);
void copy_item (const item_struct *src, item_struct *dest);
void pinup_item (cell_struct *cell);
void add_item (world_info *info, const item_struct *source, gint tab_number);
cell_struct* get_grid (world_info *info, gint tab_number, gint x, gint y);
void free_cell (world_info *info, gint tab_number, gint x, gint y);
void push_blank_cell (world_info *info, gint tab_number, gint x, gint y);
void show_empty_cell (world_info *info, gint tab_number, gint x, gint y);

